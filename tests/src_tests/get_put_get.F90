! * libdescrip - Vertical grid descriptor library for FORTRAN programming
! * Copyright (C) 2016  Direction du developpement des previsions nationales
! *                     Centre meteorologique canadien
! *
! * This library is free software; you can redistribute it and/or
! * modify it under the terms of the GNU Lesser General Public
! * License as published by the Free Software Foundation,
! * version 2.1 of the License.
! *
! * This library is distributed in the hope that it will be useful,
! * but WITHOUT ANY WARRANTY; without even the implied warranty of
! * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
! * Lesser General Public License for more details.
! *
! * You should have received a copy of the GNU Lesser General Public
! * License along with this library; if not, write to the
! * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
! * Boston, MA 02111-1307, USA.
program tests

   ! Goal      : test put method for PTOP, PREF, RC_1, RC_2
   !
   ! Strategie : we get, we put and get again to se if parameter is ok   

   use vGrid_Descriptors, only: vgrid_descriptor,vgd_new,vgd_get,vgd_put,VGD_ERROR, VGD_OK
   use Unit_Testing, only: ut_report
   
   implicit none
   
   type(vgrid_descriptor) :: vgd
   integer :: stat,lu=10,lutxt=69,ip1,ip2
   real*8 :: v1_8, v2_8 
   logical :: OK = .true.
   
   integer, external :: fnom,fstouv,fclos,fstfrm
   
   stat=fnom(lu,"data/dm_5002_from_model_run","RND",0)
   if(stat.lt.0)then
      print*,'ERROR with fnom'
      call abort()
   endif
   stat=fstouv(lu,'RND')
   if(stat.le.0)then
      print*,'No record in RPN file'
      call abort()
   endif
   open(unit=lutxt,file='data/dm_5002_ips.txt',status='OLD')
   read(lutxt,*) ip1,ip2
   close(lutxt)
   
   ! Construct a new set of 3D coordinate descriptors
   stat = vgd_new(vgd,unit=lu,format="fst",ip1=ip1,ip2=ip2)
   if(stat /= VGD_OK) OK=.false.
   
   stat = vgd_get(vgd,'PTOP',value=v1_8)
   v1_8=v1_8+1
   stat = vgd_put(vgd,'PTOP',value=v1_8)
   stat = vgd_get(vgd,'PTOP',value=v2_8)
   print*,'PTOP',v1_8, v2_8
   if(v2_8 /= v1_8)then
      print*,'OUPS PTOP v1_8 not equal to v2_8 -> ', v1_8, v2_8
      OK=.false.
   endif

   stat = vgd_get(vgd,'PREF',value=v1_8)
   v1_8=v1_8+1
   stat = vgd_put(vgd,'PREF',value=v1_8)
   stat = vgd_get(vgd,'PREF',value=v2_8)
   print*,'PREF',v1_8, v2_8
   if(v2_8 /= v1_8)then
      print*,'OUPS PREF v1_8 not equal to v2_8 -> ', v1_8, v2_8
      OK=.false.
   endif

   stat = vgd_get(vgd,'RC_1',value=v1_8)
   v1_8=v1_8+1
   stat = vgd_put(vgd,'RC_1',value=v1_8)
   stat = vgd_get(vgd,'RC_1',value=v2_8)
   print*,'RC_1',v1_8, v2_8
   if(v2_8 /= v1_8)then
      print*,'OUPS RC_1 v1_8 not equal to v2_8 -> ', v1_8, v2_8
      OK=.false.
   endif

   stat = vgd_get(vgd,'RC_2',value=v1_8)
   v1_8=v1_8+1
   stat = vgd_put(vgd,'RC_2',value=v1_8)
   stat = vgd_get(vgd,'RC_2',value=v2_8)
   print*,'RC_2',v1_8, v2_8
   if(v2_8 /= v1_8)then
      print*,'OUPS RC_2 v1_8 not equal to v2_8 -> ', v1_8, v2_8
      OK=.false.
   endif

   ! Close files
   stat = fstfrm(lu)
   stat = fclos(lu)

   call ut_report(OK,message='Grid_Descriptors::vgd_get get valid value')
      
end program tests
